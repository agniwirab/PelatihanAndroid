package com.example.agniwira.myapplication;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.agniwira.myapplication.task.MenuActivityTask;


public class MainActivity extends AppCompatActivity {
    public static final String USER_NAME = "com.example.myfirstapp.NAMEUSER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button helloButton = (Button) findViewById(R.id.buttonLogin);
        final EditText etUsername= (EditText) findViewById(R.id.edittext1);
        final EditText etPassword= (EditText) findViewById(R.id.edittext2);

        final Intent intent = new Intent(this,HomeActivity.class);


        final Context ctx = getApplicationContext();

        helloButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                if(checkPass(etPassword.getText().toString())){

                    new MenuActivityTask(MainActivity.this).execute(password,username);
                } else {
                    Toast.makeText(getApplicationContext(), "Password harus lebih dari 6 karakter", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public boolean checkPass(String pass){
        if(pass.length()>6){
            return true;
        } else {
            return false;
        }
    }




}
